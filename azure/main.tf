terraform {    
  required_providers {    
    azurerm = {    
      source = "hashicorp/azurerm"    
    }    
  }    
}    

provider "azurerm" {
	features {}
	subscription_id = "${var.subscription_id}"
	client_id       = "${var.client_id}"
	client_secret   = "${var.client_secret}"
	tenant_id       = "${var.tenant_id}"
}
  
resource "azurerm_resource_group" "azOps_rg" {    
	name = var.resource_group_name    
	location = var.location 
}
   
resource "azurerm_app_service_plan" "azOps_asp" {  
	name                = var.app_service_planName  
	location            = azurerm_resource_group.azOps_rg.location
	resource_group_name = azurerm_resource_group.azOps_rg.name  
	kind                = "Linux"
	reserved            = true
	
	sku {  
		tier = "Basic"
		size = "B1"  
	}  
}  
  
resource "azurerm_app_service" "azOps_as" {  
	name                = var.app_service
	location            = azurerm_resource_group.azOps_rg.location
	resource_group_name = azurerm_resource_group.azOps_rg.name  
	app_service_plan_id = azurerm_app_service_plan.azOps_asp.id  

	site_config {
		linux_fx_version = "DOTNETCORE|3.1"
		remote_debugging_enabled = true
		remote_debugging_version = "VS2019"
	}
}