variable "subscription_id" {
  description = "Subscription Id"
  default     = "6fa98818-fd3a-44d7-a939-84388395d2d1"
}

variable "tenant_id" {
  description = "Tenant Id"
  default     = "7708ce37-5e53-44be-927e-69b78f7fabca"
}

variable "client_id" {
  description = "Client Id"
  default     = "8d09738b-dc75-4453-aa13-2109c07503b7"
}

variable "client_secret" {
  description = "Client Secret"
  default     = "A_0kufDMfoit-APa48zRRg1ULK1PluxUwX"
}

variable "resource_group_name" {
  description = "Resource Group Name"
  default     = "azops-rsg"
}

variable "location" {
  description = "Location"
  default     = "centralus"
}

variable "app_service_planName" {
  description = "App Service PlanName"
  default     = "azops-asp"
}

variable "app_service" {
  description = "Web App Name"
  default     = "azops-webapp"
}